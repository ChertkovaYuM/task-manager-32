package ru.tsc.chertkova.tm.dto.request.project;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.dto.request.user.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class ProjectCreateRequest extends AbstractUserRequest {

    @Nullable
    private String name;

    @Nullable
    private String description;

}
