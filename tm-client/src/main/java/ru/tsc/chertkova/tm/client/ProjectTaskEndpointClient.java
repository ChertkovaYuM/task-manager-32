package ru.tsc.chertkova.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.chertkova.tm.api.endpoint.IProjectTaskEndpoint;
import ru.tsc.chertkova.tm.dto.request.task.TaskBindToProjectRequest;
import ru.tsc.chertkova.tm.dto.request.task.TaskUnbindToProjectRequest;
import ru.tsc.chertkova.tm.dto.response.task.TaskBindToProjectResponse;
import ru.tsc.chertkova.tm.dto.response.task.TaskUnbindToProjectResponse;

@NoArgsConstructor
public final class ProjectTaskEndpointClient extends AbstractEndpointClient implements IProjectTaskEndpoint {

    public ProjectTaskEndpointClient(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskBindToProjectResponse bindToProjectTaskById(@NotNull TaskBindToProjectRequest request) {
        return call(request, TaskBindToProjectResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskUnbindToProjectResponse unbindToProjectTaskById(@NotNull TaskUnbindToProjectRequest request) {
        return call(request, TaskUnbindToProjectResponse.class);
    }

}
