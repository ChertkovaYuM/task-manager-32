package ru.tsc.chertkova.tm.dto.response.project;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class ProjectClearResponse extends AbstractProjectResponse {
}
