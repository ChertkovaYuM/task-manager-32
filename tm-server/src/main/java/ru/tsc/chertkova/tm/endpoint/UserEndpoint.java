package ru.tsc.chertkova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.chertkova.tm.api.endpoint.IUserEndpoint;
import ru.tsc.chertkova.tm.api.service.IServiceLocator;
import ru.tsc.chertkova.tm.dto.request.user.*;
import ru.tsc.chertkova.tm.dto.response.user.*;

public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    public UserChangePasswordResponse changeUserPassword(@NotNull UserChangePasswordRequest request) {
        check(request);
        return new UserChangePasswordResponse();
    }

    @NotNull
    @Override
    public UserLockResponse lockUser(@NotNull UserLockRequest request) {
        check(request);
        return new UserLockResponse();
    }

    @NotNull
    @Override
    public UserRegistryResponse registryUser(@NotNull UserRegistryRequest request) {
        return new UserRegistryResponse();
    }

    @NotNull
    @Override
    public UserRemoveResponse removeUser(@NotNull UserRemoveRequest request) {
        check(request);
        return new UserRemoveResponse();
    }

    @NotNull
    @Override
    public UserUnlockResponse unlockUser(@NotNull UserUnlockRequest request) {
        check(request);
        return new UserUnlockResponse();
    }

    @NotNull
    @Override
    public UserUpdateProfileResponse updateUserProfile(@NotNull UserUpdateProfileRequest request) {
        check(request);
        return new UserUpdateProfileResponse();
    }

}
