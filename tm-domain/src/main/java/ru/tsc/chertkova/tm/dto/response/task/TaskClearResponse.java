package ru.tsc.chertkova.tm.dto.response.task;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class TaskClearResponse extends AbstractTaskResponse {

}
