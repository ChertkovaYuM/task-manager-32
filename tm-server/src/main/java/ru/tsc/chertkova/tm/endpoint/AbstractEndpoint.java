package ru.tsc.chertkova.tm.endpoint;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.service.*;
import ru.tsc.chertkova.tm.dto.request.user.AbstractUserRequest;
import ru.tsc.chertkova.tm.enumerated.Role;
import ru.tsc.chertkova.tm.exception.user.AccessDeniedException;

public abstract class AbstractEndpoint {

    @Getter
    @NotNull
    private final IServiceLocator serviceLocator;

    protected AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    protected void check(@NotNull final AbstractUserRequest request, @NotNull final Role role) {
        check(request);
    }

    protected void check(@NotNull final AbstractUserRequest request) {
        @Nullable final String userId = request.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
    }

    @NotNull
    public IPropertyService getPropertyService() {
        return getServiceLocator().getPropertyService();
    }

}
