package ru.tsc.chertkova.tm.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.tsc.chertkova.tm.dto.response.AbstractResultResponse;

@NoArgsConstructor
public final class UserRegistryResponse extends AbstractResultResponse {

    public UserRegistryResponse(@NotNull Throwable throwable) {
        super(throwable);
    }

}
