package ru.tsc.chertkova.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.dto.request.user.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class ProjectShowByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public ProjectShowByIdRequest(@Nullable String id) {
        this.id = id;
    }

}
