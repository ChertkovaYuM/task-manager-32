package ru.tsc.chertkova.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.tsc.chertkova.tm.dto.response.AbstractResultResponse;

@Getter
@Setter
@NoArgsConstructor
public final class UserChangePasswordResponse extends AbstractResultResponse {

    public UserChangePasswordResponse(@NotNull Throwable throwable) {
        super(throwable);
    }

}
