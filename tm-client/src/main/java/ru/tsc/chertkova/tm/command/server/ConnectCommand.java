package ru.tsc.chertkova.tm.command.server;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.command.AbstractCommand;
import ru.tsc.chertkova.tm.enumerated.Role;

import java.net.Socket;

public class ConnectCommand extends AbstractCommand {

    @NotNull
    public static final String NAME = "connect";

    @NotNull
    public static final String DESCRIPTION = "Connect to server.";

    @Nullable
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Override
    @SneakyThrows
    public void execute() {
        serviceLocator.getAuthEndpoint().connect();
        Socket socket = serviceLocator.getAuthEndpoint().getSocket();
        serviceLocator.getSystemEndpoint().setSocket(socket);
        serviceLocator.getProjectEndpoint().setSocket(socket);
        serviceLocator.getTaskEndpoint().setSocket(socket);
        serviceLocator.getProjectTaskEndpoint().setSocket(socket);
        serviceLocator.getDomainEndpoint().setSocket(socket);
        serviceLocator.getUserEndpoint().setSocket(socket);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }
}
