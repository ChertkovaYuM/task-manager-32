package ru.tsc.chertkova.tm.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.tsc.chertkova.tm.dto.response.AbstractResultResponse;

@NoArgsConstructor
public final class UserUpdateProfileResponse extends AbstractResultResponse {

    public UserUpdateProfileResponse(@NotNull Throwable throwable) {
        super(throwable);
    }

}
