package ru.tsc.chertkova.tm.exception.entity;

import ru.tsc.chertkova.tm.exception.AbstractException;

public final class ModelNotFoundException extends AbstractException {

    public ModelNotFoundException() {
        super("Error! Model not found...");
    }

}
