package ru.tsc.chertkova.tm.command.server;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.command.AbstractCommand;
import ru.tsc.chertkova.tm.enumerated.Role;

public class DisconnectCommand  extends AbstractCommand {

    @NotNull
    public static final String NAME = "disconnect";

    @NotNull
    public static final String DESCRIPTION = "Save backup to file.";

    @Nullable
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public void execute() {

    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }
}
