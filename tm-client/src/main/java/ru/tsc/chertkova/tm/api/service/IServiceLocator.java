package ru.tsc.chertkova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.chertkova.tm.client.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    TaskEndpointClient getTaskEndpoint();

    @NotNull
    ProjectEndpointClient getProjectEndpoint();

    @NotNull
    ProjectTaskEndpointClient getProjectTaskEndpoint();

    @NotNull
    SystemEndpointClient getSystemEndpoint();

    @NotNull
    AuthEndpointClient getAuthEndpoint();

    @NotNull
    DomainEndpointClient getDomainEndpoint();

    @NotNull
    UserEndpointClient getUserEndpoint();

}
