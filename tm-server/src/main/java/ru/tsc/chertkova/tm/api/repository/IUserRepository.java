package ru.tsc.chertkova.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    @Nullable
    User removeByLogin(@Nullable String login);

}
