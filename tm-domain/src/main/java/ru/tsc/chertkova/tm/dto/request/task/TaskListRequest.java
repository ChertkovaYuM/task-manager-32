package ru.tsc.chertkova.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.enumerated.Sort;

@Getter
@Setter
@NoArgsConstructor

public class TaskListRequest {
    @Nullable
    private Sort sort;

    public TaskListRequest(Sort sort) {
        this.sort = sort;
    }

}
