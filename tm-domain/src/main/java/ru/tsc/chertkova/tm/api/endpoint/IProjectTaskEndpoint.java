package ru.tsc.chertkova.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.chertkova.tm.dto.request.task.TaskBindToProjectRequest;
import ru.tsc.chertkova.tm.dto.request.task.TaskUnbindToProjectRequest;
import ru.tsc.chertkova.tm.dto.response.task.TaskBindToProjectResponse;
import ru.tsc.chertkova.tm.dto.response.task.TaskUnbindToProjectResponse;

public interface IProjectTaskEndpoint {

    @NotNull
    @SneakyThrows
    TaskBindToProjectResponse bindToProjectTaskById(@NotNull TaskBindToProjectRequest request);

    @NotNull
    @SneakyThrows
    TaskUnbindToProjectResponse unbindToProjectTaskById(@NotNull TaskUnbindToProjectRequest request);

}
