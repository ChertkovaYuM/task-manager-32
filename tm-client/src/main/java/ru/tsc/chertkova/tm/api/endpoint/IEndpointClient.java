package ru.tsc.chertkova.tm.api.endpoint;

public interface IEndpointClient {

    void connect();

    void disconnect();

}
